import {createAction, handleActions} from 'redux-actions';

export const UPLOAD_FILE = 'UPLOAD_FILE';
export const UPLOAD_FILE_SUCCESS = 'UPLOAD_FILE_SUCCESS';
export const UPLOAD_FILE_ERROR = 'UPLOAD_FILE_ERROR';

export const uploadFileAction = createAction(UPLOAD_FILE);
export const uploadFileSuccessAction = createAction(UPLOAD_FILE_SUCCESS);
export const uploadFileErrorAction = createAction(UPLOAD_FILE_ERROR);

const initialState = {
  report: null,
  error: null,
  uploading: false,
};

export const uploadReducer = handleActions(
  {
    [UPLOAD_FILE]: (state, action) => {
      return {
        ...state,
        uploading: true,
      };
    },

    [UPLOAD_FILE_SUCCESS]: (state, action) => {
      return {
        ...state,
        report: action.payload.data,
        error: null,
        uploading: false,
      };
    },

    [UPLOAD_FILE_ERROR]: (state, action) => {
      return {
        ...state,
        report: null,
        error: {
          status: action.payload.status,
          message: action.payload.data ? action.payload.data : 'Something went wrong',
        },
        uploading: false,
      };
    },
  },
  initialState,
);
