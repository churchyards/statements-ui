import axios from 'axios';

export const api = axios.create({
    baseURL: ``,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
    timeout: 10000,
});
