import {combineEpics, ofType} from 'redux-observable';
import {of} from 'rxjs';
import {catchError, debounceTime, map, mergeMap,} from 'rxjs/operators';
import {api} from '../api';
import {uploadFileAction, uploadFileErrorAction, uploadFileSuccessAction} from "../reducers/uploadReducer";

export const BASE_URL = '/api/v1';

const config = {
  headers: {
    'content-type': 'multipart/form-data'
  }
};

const toFormData = (file) => {
  const formData = new FormData();
  formData.append("file", file, file.name);
  return formData;
};

export const uploadFileToApi = (action$) => {
  return action$.pipe(
    ofType(uploadFileAction().type),
    debounceTime(500),
    mergeMap(action => api.post(`${BASE_URL}/reports`, toFormData(action.payload), config)),
    map(resp => uploadFileSuccessAction(resp)),
    catchError(error => of(uploadFileErrorAction(error.response)))
  );
};

export const uploadEpic = combineEpics(uploadFileToApi);


