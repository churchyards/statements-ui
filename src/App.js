import React from 'react';
import styled from 'styled-components';
import './App.css';
import {Header} from "./components/Header/Header";
import {FileUploadPage} from "./components/FileUploadPage/FileUploadPage";

const Container = styled.div`
    display: flex;
    flex-direction: column;
`;

function App() {
  return (
    <Container>
      <Header />
      <FileUploadPage />
    </Container>
  );
}

export default App;
