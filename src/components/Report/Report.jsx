import React from 'react';
import styled from 'styled-components';
import {FailureIcon} from "../FailureIcon/FailureIcon";
import {SuccessIcon} from "../SuccessIcon/SuccessIcon";
import {FailedRecordsGrid} from "../FailedRecordsGrid/FailedRecordsGrid";

const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;
  color: ${props => props.failed ? "red" : "green"};
`;

const H2 = styled.h2`
  margin-right: 30px;
`;

const SummaryTable = styled.table`
  width: 400px;
  margin-left: 20px;
`;

const Value = styled.td`
  text-align: right;
`;

const formatDateString = str => {
  const date = new Date(Date.parse(str));
  return `${date.toLocaleDateString('nl-NL')} ${date.toLocaleTimeString('nl-NL')}`;
};

export const Report = props => (
  <Container>
    {
      props.data.numberOfFailedRecords > 0 ?
        <Header failed={true}>
          <H2>Validation Failed</H2>
          <FailureIcon size={'32px'}/>
        </Header> :
        <Header failed={false}>
          <H2>Validation Succeeded</H2>
          <SuccessIcon size={'32px'}/>
        </Header>
    }

    {formatDateString(props.data.timestamp)}

    <h2>{props.data.fileName}</h2>

    <SummaryTable>
      <tbody>
      <tr>
        <td>Number of records:</td>
        <Value>{props.data.numberOfRecords}</Value>
      </tr>
      <tr>
        <td>Number of failed records:</td>
        <Value>{props.data.numberOfFailedRecords}</Value>
      </tr>
      </tbody>
    </SummaryTable>

    {
      props.data.numberOfFailedRecords > 0 ? (
        <div>
          <SummaryTable>
            <tbody>
            <tr>
              <td colSpan={2}>Caused by:</td>
            </tr>
            <tr>
              <td>
                <li>duplicate transaction reference:</li>
              </td>
              <Value>{props.data.numberOfDuplicates}</Value>
            </tr>
            <tr>
              <td>
                <li>invalid balance:</li>
              </td>
              <Value>{props.data.numberOfInvalidBalances}</Value>
            </tr>
            </tbody>
          </SummaryTable>
          <FailedRecordsGrid failedRecords={props.data.failedRecords} />
        </div>
      ) : null
    }
  </Container>
);




