import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  height: 25px;
  width: 62px;
  font-size: 14px;
  display: inline-block;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
  touch-action: manipulation;
  cursor: ${props => props.disabled ? "default" : "pointer"};
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  border: 0;
  border-radius: 2px;
  color: #fff;
  outline: 0;
  background-color: ${props => props.disabled ? "#bdbdbd" : "#673ab7"};
`;

export const Button = props => (
  <StyledButton
    onClick={props.onClick}
    disabled={props.disabled}
  >{props.children}
  </StyledButton>
);

