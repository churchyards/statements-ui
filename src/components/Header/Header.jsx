import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    width: 100%;
    height: 64px;
    background-color: #673ab7;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: left;
`;

const Title = styled.span`
  color: white;
  font-size: 28px;
  font-weight: bold;
  margin: 16px; 
`;

export const Header = () => (
  <Container>
    <Title>Customer Statements</Title>
  </Container>
);


