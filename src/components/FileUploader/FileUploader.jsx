import React from 'react';
import styled from 'styled-components';
import {DropZone} from "../DropZone/DropZone";
import {Button} from "../Button/Button";

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

const Actions = styled.div`
  flex: 1;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 32px;
`;

const File = styled.span`
  font-size: 16px;
  color: #555;
  margin-right: 10px;
`;

export class FileUploader extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      file: null,
    };

    this.handleOnFileAdded = this.handleOnFileAdded.bind(this);
    this.handleOnClickUploadButton = this.handleOnClickUploadButton.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.uploading === false) {
      this.setState({ file: null });
    }
  }

  handleOnFileAdded(file) {
    this.setState({ file: file});
  }

  handleOnClickUploadButton() {
    this.props.onUploadFile(this.state.file);
  }

  render() {
    return (
      <Container>
        <DropZone onFileAdded={this.handleOnFileAdded}
                  disabled={this.state.file}
        />
        <Actions>
          <File>{this.state.file ? this.state.file.name : ''}</File>
          <Button
            onClick={this.handleOnClickUploadButton}
            disabled={!this.state.file}
          >Upload
          </Button>
        </Actions>
      </Container>
    )
  }
}




