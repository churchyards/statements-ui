import React from 'react';
import styled from 'styled-components';
import { keyframes }  from 'styled-components';

const show = keyframes`
from {
  transform: translateY(10px);
  opacity: 0;
}

to {
  transform: translateY(0);
  opacity: 1;
}
`;

const hide = keyframes`
  from {
    transform: translateY(0);
    opacity: 1;
  }

  to {
    transform: translateY(10px);
    opacity: 0;
  }
`;

const Container = styled.div`
    background-color: red;
    color: white;
    box-shadow: 0 1px 7px rgba(0, 0, 0, 0.3);
    border-radius: 4px;
    line-height: 24px;
    font-size: 16px;
    padding: 16px 32px;
    margin: 32px;
    z-index: 9999;
    animation: ${props => (props.isVisible ? show : hide)} 0.5s ease forwards;
`;

export class Toast extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isVisible: true,
    };
  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ isVisible: false });
    }, 5000);
  }

  render() {
    return (
      <Container isVisible={this.state.isVisible}>
        {this.props.message}
      </Container>
    );
  }
}
