import React from 'react';
import styled from 'styled-components';

const StyledTable = styled.table`
  width: 100%;
  border-collapse: collapse;
  margin-top: 20px;
`;

const TD = styled.td`
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
`;

const TR = styled.tr`
  :nth-child(even) {
    background-color: #dddddd;
  }  
`;

export const FailedRecordsGrid = props => (
  <StyledTable>
    <tr>
      <TD><b>Reference</b></TD>
      <TD><b>Account</b></TD>
      <TD><b>Start balance</b></TD>
      <TD><b>Mutation</b></TD>
      <TD><b>End balance</b></TD>
      <TD><b>Description</b></TD>
      <TD><b>Error</b></TD>
    </tr>
    <tbody>
      {
        props.failedRecords.map( f => (
            <TR>
              <TD>{f.transactionReference}</TD>
              <TD>{f.accountNumber}</TD>
              <TD>{f.startBalance}</TD>
              <TD>{f.mutation}</TD>
              <TD>{f.endBalance}</TD>
              <TD>{f.description}</TD>
              <TD>{f.invalidBalance ? 'INVALID BALANCE' : 'DUPLICATE'}</TD>
            </TR>
          )
        )
      }
    </tbody>
  </StyledTable>
);




