import React from 'react';
import styled from 'styled-components';
import {connect} from 'react-redux';
import {FileUploader} from "../FileUploader/FileUploader";
import {ReportViewer} from "../ReportViewer/ReportViewer";
import {uploadFileAction} from "../../reducers/uploadReducer";
import {Toast} from "../Toast/Toast";

const Container = styled.div`
    display: flex;
    flex-direction: row;
`;

const Content = styled.div`
  width: 100%;
  min-height: 90vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
  background-color: rgb(206, 213, 223);
  font-size: calc(10px + 2vmin);
`;

const Card = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  background-color: white;
  padding: 64px;
  // padding: 32px;
  // width: 50%;
  box-shadow: 0 15px 30px 0 rgba(0, 0, 0, 0.11), 0 5px 15px 0 rgba(0, 0, 0, 0.08);
  box-sizing: border-box;  
`;

export class FileUploadPageComponent extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      isReportViewerOpen: false,
    };

    this.handleOnCloseReportViewer = this.handleOnCloseReportViewer.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.uploading && nextProps.report) {
      this.setState({ isReportViewerOpen: true });
    }
  }

  handleOnCloseReportViewer() {
    this.setState({ isReportViewerOpen: false });
  }

  render() {
    return (
        <Container>
          <Content>
            <Card>
              <FileUploader uploading={this.props.uploading} onUploadFile={this.props.handleOnUploadFile}  />
            </Card>
            {
              this.props.error ?
                <Toast
                  message={ this.props.error.message }
                /> : null
            }
          </Content>
          <ReportViewer report={this.props.report}
                        isReportViewerOpen={this.state.isReportViewerOpen}
                        onClose={this.handleOnCloseReportViewer}
          />
        </Container>
    )
  }
}

const mapStateToProps = state => {
  return ({
    report: state.upload.report,
    error: state.upload.error,
    uploading: state.upload.uploading,
  });
};

const mapDispatchToProps = dispatch => ({
  handleOnUploadFile: (file) => {
    dispatch(uploadFileAction(file));
  },
});

export const FileUploadPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(FileUploadPageComponent);



