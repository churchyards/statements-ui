import React from 'react';
import {Report} from "../Report/Report";
import SlidingPane from 'react-sliding-pane';
import 'react-sliding-pane/dist/react-sliding-pane.css';
import './ReportViewer.css';

export const ReportViewer = props => (
  <SlidingPane
    className={"slidePanel"}
    isOpen={ props.isReportViewerOpen }
    onRequestClose={ props.onClose }
  >
    {
      props.report ? <Report data={props.report} /> : <span>No file uploaded</span>
    }
  </SlidingPane>
);





