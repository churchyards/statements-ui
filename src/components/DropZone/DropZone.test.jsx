import React from 'react';
import {expect} from 'chai';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {DropZone} from "./DropZone";

configure({ adapter: new Adapter() });

describe('DropZone', () => {

  let underTest;

  beforeEach(() => {
    const props = {
      disabled: false,
      onFilesAdded: jest.fn(),
    };
    underTest = mount(<DropZone {...props} />);
  });

  it('renders an enabled and not highlighted DropZone', () => {
    const container = underTest.find('div');
    expect(container.props().disabled).to.equal(false);
    expect(underTest.state('highlight')).to.equal(false);
  });


  it('onDragOver highlights the DropZone', () => {
    underTest.simulate('dragover');
    expect(underTest.state('highlight')).to.equal(true);
  });
});

