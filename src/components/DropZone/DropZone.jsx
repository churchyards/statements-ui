import React from 'react';
import styled from 'styled-components';
import {UploadIcon} from "../UploadIcon/UploadIcon";

const Container = styled.div`
  height: 200px;
  width: 200px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  // background-color: #fff;
  font-size: 16px;
  border: 2px dashed rgb(187, 186, 186);
  border-radius: 50%;
  cursor: ${props => props.disabled ? "default" : "pointer"};
`;

const HiddenFileInput = styled.input`
  display: none;
`;

export class DropZoneComponent extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = { highlight: false };
    this.fileInputRef = React.createRef();

    this.openFileDialog = this.openFileDialog.bind(this);
    this.onFileSelected = this.onFileSelected.bind(this);
    this.onDragOver = this.onDragOver.bind(this);
    this.onDragLeave = this.onDragLeave.bind(this);
    this.onDrop = this.onDrop.bind(this);
  }

  openFileDialog() {
    if (this.props.disabled) return;
    this.fileInputRef.current.click();
  }

  onFileSelected(evt) {
    if (this.props.disabled) return;
    const files = evt.target.files;
    this.onFileAdded(files);
  }

  onDrop(event) {
    event.preventDefault();
    if (this.props.disabled) return;
    const files = event.dataTransfer.files;
    this.onFileAdded(files);
    this.setState({ highlight: false });
  }

  onDragOver(evt) {
    evt.preventDefault();
    if (this.props.disabled) return;
    this.setState({ highlight: true });
  }

  onDragLeave() {
    this.setState({ highlight: false });
  }

  fileListToArray(list) {
    const array = [];
    for (var i = 0; i < list.length; i++) {
      array.push(list.item(i));
    }
    return array;
  }

  onFileAdded(files) {
    if (this.props.onFileAdded) {
      const array = this.fileListToArray(files);
      if (array[0]) {
        this.props.onFileAdded(array[0]);
      }
    }
  }

  render() {
    return (
      <Container
        style={{backgroundColor: this.state.highlight ? "#bcb9ec" : "#fff"}}
        onClick={this.openFileDialog}
        onDrop={this.onDrop}
        onDragOver={this.onDragOver}
        onDragLeave={this.onDragLeave}
        disabled={this.props.disabled}
      >
        <UploadIcon
          opacity={0.3}
          size={'64px'}
        />
        <HiddenFileInput
          ref={this.fileInputRef}
          type="file"
          onChange={this.onFileSelected}
        />
        <span>Upload File</span>
      </Container>
    )
  }
}

export const DropZone = DropZoneComponent;


