import {applyMiddleware, combineReducers, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import {combineEpics, createEpicMiddleware} from 'redux-observable';
import {uploadReducer} from "./reducers/uploadReducer";
import {uploadEpic} from "./epics/uploadEpic";

const rootReducer = combineReducers({
  upload: uploadReducer,
});

const epicMiddleware = createEpicMiddleware();

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(epicMiddleware)));

const allEpics = combineEpics(uploadEpic);

epicMiddleware.run(allEpics);
