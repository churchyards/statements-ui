## Customer Statement Processor Front-End

This project contains a user interface that can be used to upload XML and CSV statement files to the 
statements-service API. 
The user can drag and drop files to a drop zone. Alternatively, the drop zone can be clicked and a 
file can be selected in a file dialog. By pressing Upload button the file is posted to the API and the
validation report is shown. 

In the project directory, you can run:

### `npm install`
 
### `npm start`

Open in browser:

### `localhost:3000/`

Test files are provided in statements-service project for several scenario's:
- a file with duplicate records
- a file with records with invalid balances
- a file with mixed invalid records
- a valid file
- an empty file that results into an API error

A unit test (only one) can be run:

### `npm test`

Its is assumed that the API runs at localhost:8080, or the proxy must be changed in package.json.

 

